magit-popup (2.13.3-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 22:40:20 +0900

magit-popup (2.13.3-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Nicholas D Steeves ]
  * Drop emacs24 and emacs25 from Enhances (packages do not exist in
    bullseye).

  [ Xiyue Deng ]
  * Repair d/watch.
  * New upstream release (Closes: #1041822).
  * Drop Built-Using from elpa-magit-popup as per lintian suggestion.
  * Update Standards-Version to 4.6.2.  No change needed.
  * Add Upstream-Contact in d/copyright.
  * Update debhelper-compat to 13.

 -- Xiyue Deng <manphiz@gmail.com>  Fri, 20 Oct 2023 17:03:24 -0700

magit-popup (2.13.2-1) unstable; urgency=medium

  [ David Krauser ]
  * Update maintainer email address

  [ Rémi Vanicat ]
  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Tue, 14 Jan 2020 14:07:28 +0100

magit-popup (2.13.0-1) unstable; urgency=medium

  * New upstream version
  * Change control: magit-popup is deprecated by upstream, and goes in
    maintenance only mode

 -- Rémi Vanicat <vanicat@debian.org>  Mon, 29 Jul 2019 09:44:42 +0200

magit-popup (2.12.5-1) unstable; urgency=medium

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 28 Dec 2018 11:48:45 +0100

magit-popup (2.12.4-1) unstable; urgency=medium

  * New upstream version

 -- Rémi Vanicat <vanicat@debian.org>  Sun, 29 Jul 2018 12:20:55 +0200

magit-popup (2.12.3-2) unstable; urgency=medium

  * Add attributions to debian/copyright (Closes: #900835)

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 08 Jun 2018 21:39:30 +0200

magit-popup (2.12.3-1) unstable; urgency=medium

  * First release (Closes: #895010).

 -- Rémi Vanicat <vanicat@debian.org>  Fri, 06 Apr 2018 11:11:15 +0200
